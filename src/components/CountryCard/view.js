import React from "react";
import { Link } from 'react-router-dom';

import { countryPropTypes } from "../../constants/countryPropTypes";

import './styles.scss';

function CountryCard({ name, capital, population, flag }) {
  return (
    <li className="CountryCard__card">
      <Link to={`/country/${name}`}>
        <h3>{name}</h3>
        <img src={flag} alt={name} />
        <h4>{capital}</h4>
        <h4>{population}</h4>
      </Link>
    </li>
  );
}

CountryCard.propTypes = countryPropTypes.isRequired;

export default CountryCard;