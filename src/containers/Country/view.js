import React, { useState } from 'react';

import FetchButton from '../../components/FetchButton';
import CountryList from '../../components/CountryList';

function Country() {
    const [countryList, setCountryList] = useState([]);

    function addNewCountry(country) {
       
        setCountryList([...countryList, country]);
    }

    return (
        <div className="App">
            <h1>Find a Country</h1>
            <FetchButton
                initialCounter= { countryList.length }
                onFetch= { addNewCountry }
            />
            <CountryList list={ countryList } />
        </div>
    );
}

export default Country;