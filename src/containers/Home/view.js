import React from 'react';

import { Link } from 'react-router-dom';

import './styles.scss'

import logo from '../../logo.svg'

function Home() {
    return (
    <>
    <div className="App">
      <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
        <ul>
            <li>
                <Link to="/country">Search for a Country</Link>
            </li>
        </ul>
    </>
        );
    }

export default Home;