/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { getCountryById } from "../../services/countriesApi";

import "./styles.scss";

function FetchButton({ initialCountry, onFetch }) {
  const [countries, setCountry] = useState(initialCountry);

  useEffect(() => {
    async function fetchCountry() {
      const country = await getCountryById(countries);
      onFetch(country);
    }

    if (countries) {
      fetchCountry();
    }
  }, [countries]);
//return (
     // <section class="search">
     //     <form onSubmit={event => this.onSubmit(event)}>
     //         <h2>Country search engine</h2>
     //             <input 
     //               id="country-name" 
     //               placeholder="e.g. Poland" 
     //               type="text" 
     //               onChange={event => this.onChangeHandle(event)}
     //               value={this.state.searchText}
     //                   />
     //       </form>
     //   </section>
  //);
}

FetchButton.propTypes = {
  initialCountry: PropTypes.number.isRequired,
  onFetch: PropTypes.func.isRequired
};

export default FetchButton;