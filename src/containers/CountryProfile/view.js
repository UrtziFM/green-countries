import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import CountryCard from '../../components/CountryCard';

import { getCountryById } from '../../services/countriesApi';

function CountryProfile({ match }) {
    const [country, setCountry] = useState(null);

    const { params } = match;
    const { id } = params;

    useEffect(() => {
        async function fetchCountry() {
            const country = await getCountryById(id);
            setCountry(country);
        }

        fetchCountry();
    }, [id]);

    return country ? <CountryCard {...country} /> : <h3>loading country... </h3>;
}

CountryProfile.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            id: PropTypes.string,
        })
    }).isRequired,
}

export default CountryProfile;