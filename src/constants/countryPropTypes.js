import PropTypes from 'prop-types';

export const countryPropTypes = PropTypes.shape({
  name: PropTypes.string,
  capital: PropTypes.string,
  population: PropTypes.number,
  flag: PropTypes.string,
});

export const arrayOfCountryPropTypes = PropTypes.arrayOf(countryPropTypes);