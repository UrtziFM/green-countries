import React from "react";

import { arrayOfCountryPropTypes } from "../../constants/countryPropTypes";

import CountryCard from "../CountryCard";

import "./styles.scss";

function CountryList({ list }) {
  return (
    <ul className="CountryList__main-list">
      {list.map(country => (
        <CountryCard key={country.id} {...country} />
      ))}
    </ul>
  );
}

CountryList.propTypes = {
  list: arrayOfCountryPropTypes.isRequired
};

export default CountryList;