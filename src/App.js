import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';


import './App.css';

import Home from './containers/Home';
import CountryProfile from './containers/CountryProfile';
import Country from './containers/Country';


function App() {
  return (
    <Router>
    <Switch>
      <Route path="/country" exact component={Country} />
      <Route path="/country/:id" exact component={CountryProfile} />
      <Route path="/" component={Home} />
    </Switch>
  </Router>
  )

}

export default App;
